// import base64 from './utils';

/** @type {import('./mockAPI').IMockRequestConfig[]} mockData */
const mockData = [
  {
    url: '/test-get',
    type: 'GET',
    response(config) {
      return {
        code: 200,
        msg: '',
        data: ''
      }
    }
  }
];

export default mockData;
