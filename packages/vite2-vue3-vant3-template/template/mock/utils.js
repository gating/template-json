/**
 * @description base64 加密、解密
 */

const _keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

/**
 * @description 加密 -> base64
 * @param {String} string
 * @returns {String}
 */
function _utf8_encode(string) {
  string = string.replace(/\r\n/g, '\n');
  let utftext = '';
  for (let n = 0; n < string.length; n++) {
    let c = string.codePointAt(n);
    if (c < 128) {
      utftext += String.fromCodePoint(c);
    } else if (c > 127 && c < 2048) {
      utftext += String.fromCodePoint((c >> 6) | 192);
      utftext += String.fromCodePoint((c & 63) | 128);
    } else {
      utftext += String.fromCodePoint((c >> 12) | 224);
      utftext += String.fromCodePoint(((c >> 6) & 63) | 128);
      utftext += String.fromCodePoint((c & 63) | 128);
    }
  }
  return utftext;
}

/**
 * @description 解码 -> utf8
 * @param {String} utftext
 * @returns {String}
 */
function _utf8_decode(utftext) {
  let string = '';
  let i = 0;
  let c = 0;
  let c1 = 0;
  let c2 = 0;
  let c3 = 0;
  while (i < utftext.length) {
    c = utftext.codePointAt(i);
    if (c < 128) {
      string += String.fromCodePoint(c);
      i++;
    } else if (c > 191 && c < 224) {
      c2 = utftext.codePointAt(i + 1);
      string += String.fromCodePoint(((c & 31) << 6) | (c2 & 63));
      i += 2;
    } else {
      c2 = utftext.codePointAt(i + 1);
      c3 = utftext.codePointAt(i + 2);
      string += String.fromCodePoint(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;
    }
  }
  return string;
}

const base64 = {
  decode: function (input) {
    let output = '',
      i = 0;
    let chr1, chr2, chr3;
    let enc1, enc2, enc3, enc4;
    input = input.replace(/[^A-Za-z0-9+\/=]/g, '');
    while (i < input.length) {
      enc1 = _keyStr.indexOf(input.charAt(i++));
      enc2 = _keyStr.indexOf(input.charAt(i++));
      enc3 = _keyStr.indexOf(input.charAt(i++));
      enc4 = _keyStr.indexOf(input.charAt(i++));
      chr1 = (enc1 << 2) | (enc2 >> 4);
      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
      chr3 = ((enc3 & 3) << 6) | enc4;
      output = output + String.fromCodePoint(chr1);
      if (enc3 !== 64) {
        output = output + String.fromCodePoint(chr2);
      }
      if (enc4 !== 64) {
        output = output + String.fromCodePoint(chr3);
      }
    }
    output = _utf8_decode(output);
    return output;
  },

  encode: function (input) {
    let output = '',
      i = 0;
    let chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    input = _utf8_encode(input);
    while (i < input.length) {
      chr1 = input.codePointAt(i++);
      chr2 = input.codePointAt(i++);
      chr3 = input.codePointAt(i++);
      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;
      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }
      output =
        output +
        _keyStr.charAt(enc1) +
        _keyStr.charAt(enc2) +
        _keyStr.charAt(enc3) +
        _keyStr.charAt(enc4);
    }
    return output;
  }
};

export default base64;
