import Mock from 'mockjs';
import list from './index';

Mock.setup({ timeout: '400-800' }); // delay

export default function initMockAPI() {
  // mockAPI({ url: '/test-get' }); // get (+ query)

  // mockAPI({ url: '/test-get-restful/:id/:name' }); // get + restful

  // // post
  // mockAPI({
  //   url: '/test-post',
  //   type: 'post',
  //   response(config) {
  //     console.log('config => ', config);
  //     return { list: [] };
  //   }
  // });

  // // put
  // mockAPI({
  //   url: '/test-put',
  //   type: 'put',
  //   response(config) {
  //     console.log('config => ', config);
  //     return { list: [] };
  //   }
  // });

  // // delete
  // mockAPI({ url: '/test-delete/:id', type: 'delete' });

  // console.log(list);
  list.forEach(mockAPI);
}

/** @typedef {'GET'|'POST'|'PUT'|'DELETE'} IResquestType */

/**
 * @typedef IRequestConfig
 * @property {String} url
 * @property {IResquestType} type
 * @property {Object} params
 * @property {String} body
 * @property {Object} query
 */

/**
 * @typedef IResponseResult
 * @property {Number} code
 * @property {Object|null} data
 * @property {String} msg
 */

/** @typedef {(cofig: IRequestConfig) => IResponseResult} IResponse */

/**
 * @typedef {{url: String, type: IResquestType, response: IResponse}} IMockRequestConfig
 */

/**
 * @param {IMockRequestConfig} requestConfig
 */
function mockAPI(requestConfig) {
  let { type = 'GET', url, response } = requestConfig;
  const baseURL = import.meta.env.VITE_APP_BASE_URL;
  url = baseURL + url;
  type = type.toLowerCase();
  const urlReg = path2Reg(url);
  // console.log(urlReg, url);
  Mock.mock(urlReg, type, (options) => {
    // console.log(`urlReg %c ${urlReg}  ${options.url}`, 'font-size: 16px;color: blue;');
    // console.log(`${type} ${url} => `, options);
    /** @type {IRequestConfig} requestConfig */
    const requestConfig = {
      url: options.url,
      type,
      params: parseParams(options.url, urlReg),
      body: options.body,
      query: parseQuery(options.url)
    };
    console.log('requestConfig => ', requestConfig);
    const resData = response?.(requestConfig) || null;
    const defaultData = {
      msg: `${options.url} ${type} test msg`,
      data: null,
      code: 200
    };
    return Object.assign(defaultData, resData);
  });
}

function path2Reg(url) {
  url = url.replace('?', '');
  return new RegExp(`^${params2Reg(url)}\\??([^#&*]+=([^#&*]+)?&?)*$`);
}

/**
 * @param {String} url
 */
function params2Reg(url) {
  return url.replace(/\/:(\w+)/g, '/(?<$1>[\\w-_]+)');
}

/**
 * @param {String} url
 * @param {RegExp} reg
 */
function parseParams(url, reg) {
  return reg.exec(url)?.groups || {};
}

/**
 * @param {String} url
 */
function parseQuery(url) {
  const index = url.indexOf('?');
  if (index === -1) return {};
  url = url.slice(index + 1);
  const query = {};
  url.replace(/([^&*]+)=([^&*]+)/g, (_, $1, $2) => {
    query[$1] = $2;
  });
  return query;
}

/* axios request
  await axios.get('/test-get?id=123&name=zhangsan');
  await axios.get('/test-get');
  await axios.get('/test-get-restful/123/zhangsan');
  await axios.post('/test-post');
  await axios.post('/test-post', { id: 123, name: 'zhangsan' });
  await axios.put('/test-put', { id: 456, name: 'lisi' });
  await axios.delete('/test-delete/123');
*/
