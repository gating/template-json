module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
    'vue/setup-compiler-macros': true
  },
  extends: ['eslint:recommended', 'plugin:vue/vue3-recommended', 'plugin:prettier/recommended'],
  parser: 'vue-eslint-parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  plugins: ['vue'],
  rules: {
    'vue/script-setup-uses-vars': 'warn',
    'vue/html-self-closing': [
      1,
      {
        html: {
          void: 'always',
          normal: 'always',
          component: 'always'
        },
        svg: 'always',
        math: 'always'
      }
    ],
    /* 组件未使用 */
    'vue/no-unused-components': [
      1,
      {
        ignoreWhenBindingPresent: true
      }
    ],
    'vue/html-closing-bracket-newline': [
      1,
      {
        singleline: 'never',
        multiline: 'always'
      }
    ],
    'vue/no-use-v-if-with-v-for': [
      'error',
      {
        allowUsingIterationVar: true
      }
    ],
    'vue/multi-word-component-names': [
      0,
      {
        ignores: []
      }
    ],
    'no-dupe-keys': 2,
    'no-unused-vars': 1,
    'array-bracket-newline': 1
  }
};
