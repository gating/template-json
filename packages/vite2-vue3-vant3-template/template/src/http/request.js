import axios from 'axios';

console.log(import.meta.env);

const baseURL = import.meta.env.VITE_APP_BASE_URL;

const service = axios.create({
  baseURL
});

service.interceptors.request.use(
  (config) => {
    return config;
  },
  (err) => Promise.reject(err)
);

service.interceptors.response.use(
  (res) => {
    return res.data;
  },
  (err) => Promise.reject(err)
);

export { service };

export function request(type, url, config) {
  return service({
    url,
    method: type.toLowerCase(),
    ...config
  });
}

export function get(url, params, options) {
  return request('get', url, { params, ...options });
}

// post
export function post(url, param, options) {
  return request('post', url, { data: param, ...options });
}

// put
export function put(url, param, options) {
  return request('put', url, { data: param, ...options });
}
