import { createRouter, createWebHashHistory } from 'vue-router';

import Home from '../views/Home/index.vue';

/** @type {import('vue-router').RouteRecordRaw[]} routes */
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About/index.vue')
  }
];

export default createRouter({
  history: createWebHashHistory('/'),
  routes
});
