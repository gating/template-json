# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.1.8"></a>

# 1.1.8 (2022-06-23)
[完整变更日志](https://github.com/GATING/gating-cli-template/compare/v1.1.7...v1.1.8)

## ⚡ 其他

* fix/bug ([2b014d4](https://github.com/GATING/gating-cli-template/commit/2b014d4))
* fix/postcss-px-to-viewporrt comment invalid ([9e86283](https://github.com/GATING/gating-cli-template/commit/9e86283))
* init template ([9f40fb6](https://github.com/GATING/gating-cli-template/commit/9f40fb6))

## ✨ 新功能

* template: 添加vite2-vue3-vant3-template模板发布 ([f3be26d](https://github.com/GATING/gating-cli-template/commit/f3be26d))
